window.addEventListener('load', function(){
  const canvas = this.document.getElementById('canvas1');
  const ctx = canvas.getContext('2d');
  canvas.width = 720;
  canvas.height = 720;

  class InputHandler {
    constructor(game){
      this.game = game;
      window.addEventListener('keydown', e => {
        if (( (e.key === 'ArrowUp') || 
              (e.key === 'ArrowDown')
          ) && this.game.keys.indexOf(e.key) === -1){
            this.game.keys.push(e.key);
        }
        // console.log(e.key);
        console.log(this.game.keys);
      })
      window.addEventListener('keyup', e => {
        if (this.game.keys.indexOf(e.key) > -1){
          this.game.keys.splice(this.game.keys.indexOf(e.key), 1);
        }
        console.log(this.game.keys);
      })
    }
  }

  class Projectile {

  }

  class Particle {

  }

  class Player {
    constructor(game){
      this.game = game;
      this.width = 120;
      this.height = 190;
      this.x = 20;
      this.y = 100;
      this.speedX = 0.2;
      this.speedY = 0;
    }
    update(){
      if (this.game.keys.includes('ArrowUp')){
        this.speedY = -1;
      } else if (this.game.keys.includes('ArrowDown')){
        this.speedY = 1;
      }
      this.y += this.speedY;
    }
    draw(context){
      context.fillRect(this.x, this.y, this.width, this.height); // draw a representation of the player

    }

  }

  class Enemy {

  }

  class Layer {

  }

  class Background {

  }

  class UI {

  }

  class Game {
    constructor(width, height){ // ref. that we convet to class properies
      // all in constructor will run when we instancied our game
      this.width = width;
      this.height = height;
      this.player = new Player(this); // create an instance of Player, & allow to run all the code inside the class Player's constructor
      this.input = new InputHandler(this);
      this.keys = [];
    }
    update(){
      this.player.update()
    }
    draw(context){
      this.player.draw(context); // we render the player on canvas by draw() method
    }
  }

  const game = new Game(canvas.width, canvas.height);
  /*  this new keywords triggers a class 
      constructor, 
      & when the constructor get executed, we edit constructor class code lines (here, Game constructor, with width, height properties ...) 
      this automatically create an instance of Player class when we create the `player` propertie from `new Player` 
  */
  console.log(game);

  // animation loop
  function animate(){
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    game.update();
    game.draw(ctx); // ctx is passed here
    /**
     * we want trigger the next animation frame
     * use:`requestAnimationFrame();`
    
     */
    requestAnimationFrame(animate);
  }
  animate();
})